<?php

if ( !defined( 'ABSPATH' ) ) exit;

add_shortcode( 'phut_css_check', 'phut_shortcode_styles_check' );
function phut_shortcode_styles_check(){
  ob_start();
?>
<h1>h1. Heading 1 <small>with some small text</small></h1>
<h2>h2. Heading 2 <small>with some small text</small></h2>
<h3>h3. Heading 3 <small>with some small text</small></h3>
<h4>h4. Heading 4 <small>with some small text</small></h4>
<h5>h5. Heading 5 <small>with some small text</small></h5>
<h6>h6. Heading 6 <small>with some small text</small></h6>
<hr/>
<h1>h1. Heading 1 <small>with some small text</small></h1>
<p>Make a paragraph stand out by adding a <code>.lead</code> class attribute. i.e. <code>&lt;p class="lead"&gt;...&lt;/p&gt;</code>.</p>
<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec libero sed mi fringilla vehicula. Proin pellentesque sodales enim eu aliquet. Duis quis nisl nunc. Donec ornare velit id nisi eleifend pretium. Praesent dictum nulla ac massa adipiscing, vel tempus magna commodo. Maecenas lacinia in mauris sit amet tristique.</p>
<p>In consectetur diam id augue consequat, uis scelerisque odio dapibus. Donec mattis ligula sit amet nisl ultricies, eget aliquam ante vehicula. Ut non egestas sapien. Nam euismod sollicitudin dolor, eget aliquam magna <a href="#" title="This is a link">this is a link</a> ullamcorper ac. Sed id libero sed quam porttitor aliquam eget in tellus.</p>  
<p>Curabitur nec libero vitae nibh varius accumsan. Duis pellentesque mauris tellus, non pretium tellus tempus at. In id lacinia lorem, id vehicula massa. In vitae est semper, faucibus orci at, accumsan ex. Sed vestibulum sodales enim. Donec vitae nunc laoreet, varius massa placerat, sodales lorem.</p>
<h2>h2. Heading 2 <small>with some small text</small></h2>  
<p>Mauris non rutrum metus. Nam sed nunc ut odio blandit scelerisque. Pellentesque sagittis placerat lacus, quis mollis justo facilisis id. In mattis mauris non pharetra egestas. Phasellus sodales ipsum lorem, id auctor felis malesuada ut. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel metus quis quam imperdiet blandit vel quis diam. Etiam in tincidunt elit, vel dignissim metus.</p>
<h3>h3. Heading 3 <small>with some small text</small></h3>
<p>In hac habitasse platea dictumst. Etiam ac sagittis urna, a fermentum augue. Fusce mattis lacus sed porttitor congue. Donec gravida tellus vel fermentum maximus. Integer posuere maximus eleifend. Nullam magna quam, pharetra a odio non, feugiat congue ligula. Nam nisl libero, vehicula ut arcu eget, placerat facilisis lectus.</p>
<h4>h4. Heading 4 <small>with some small text</small></h4>
<p>Sed enim mauris, aliquam ac lorem id, venenatis mollis orci. Ut fermentum at libero ac convallis. Pellentesque eleifend nisl non bibendum maximus. In mattis molestie maximus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam ac sem a ex egestas pretium quis eget nunc. Suspendisse urna massa, congue in dapibus a, porttitor eu ex. Nulla sem tellus, venenatis ac mauris et, scelerisque ultricies mauris. In hac habitasse platea dictumst.</p>
<h5>h5. Heading 5 <small>with some small text</small></h5>
<p>Pellentesque lorem diam, maximus vel nunc nec, facilisis efficitur justo. Proin vel vestibulum risus. Suspendisse eu mollis dolor. Cras aliquam sodales ligula quis molestie. Curabitur ex nunc, luctus ut nisi condimentum, suscipit scelerisque dui. Donec varius, ipsum a dignissim vehicula, lorem arcu pharetra erat, et sodales lectus lorem non sapien. Nunc a ornare tellus.</p>
<h6>h6. Heading 6 <small>with some small text</small></h6>
<p><small>Praesent viverra arcu enim, sed iaculis purus dapibus at. Phasellus in erat accumsan, rutrum risus eget, tempor mi. Sed eget mauris diam. Phasellus pharetra ante velit, vitae euismod purus facilisis pulvinar. Vestibulum pellentesque, neque nec sodales sagittis, tortor ante cursus arcu, at vehicula dolor metus nec nulla. Pellentesque elementum nunc at sagittis auctor. Sed eu nulla in orci vehicula auctor.</small></p>
<hr />

<h2>Images</h2>

<h3>Align None</h3>
<div class="clearfix">
<img src="https://placehold.it/300x150" alt="Alternate Text" title="Title Text" class="alignnone">
</div>

<h3>Align Left</h3>
<div class="clearfix">
<img src="https://placehold.it/300x150" alt="Alternate Text" title="Title Text" class="alignleft">
</div>

<h3>Align Right</h3>
<div class="clearfix">
<img src="https://placehold.it/300x150" alt="Alternate Text" title="Title Text" class="alignright">
</div>

<h3>Align Centre</h3>
<div class="clearfix">
<img src="https://placehold.it/300x150" alt="Alternate Text" title="Title Text" class="aligncenter">
</div>

<h3>Linked Image</h3>
<div class="clearfix">
<a href="#"><img src="https://placehold.it/300x150" alt="Alternate Text" title="Title Text" class="alignnone"></a>
</div>

<h2>Lists</h2>
<h3>Nested unordered lists</h3>
<ul>
<li>List item</li>
<li>List item<ul>
<li>List item</li>
<li>List item<ul>
<li>List item</li>
<li>List item</li>
<li>List item
<ul>
<li>List item</li>
<li>List item</li>
</ul>
</li>
<li>List item</li>
<li>List item</li>
</ul></li>
<li>List item</li>
</ul></li>
<li>List item</li>
<li>List item</li>
</ul>
<h3>Nested ordered lists</h3>
<ol>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item</li>
</ol></li>
<li>List item</li>
<li>List item</li>
</ol></li>
<li>List item</li>
</ol></li>
<li>List item</li>
<li>List item</li>
</ol>
<h3>Nested mixed lists</h3>
<ul>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item</li>
<li>List item<ul>
<li>List item</li>
<li>List item</li>
</ul></li>
</ol></li>
<li>List item</li>
<li>List item</li>
</ol></li>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item<ol>
<li>List item</li>
<li>List item</li>
<ul>
<li>List item</li>
<li>List item</li>
<li>List item</li>
</ul></ol></li>
</ol></li>
<li>List item</li>


</ul>
<h2>Inline Text Elements</h2>
<h3>Marked Text</h3>
<p>You can use the <code>&lt;mark&gt;...&lt;/mark&gt;</code> tag to highlight text. <mark>This section of text is meant to be highlighted.</mark></p>
<h3>Cited Text</h3>
<p><code>&lt;cite&gt;...&lt;/cite&gt;</code> is used to define the title of a work (e.g. a book, a song, a movie, a TV show, a painting, a sculpture, etc.).  For example <cite>The Wind In The Willows</cite> by Kenneth Grahame.</p><p><strong>Note:</strong> A person's name is not the title of a work.</strong></p>
<h3>Deleted Text</h3>
<p>For indicating blocks of text that have been deleted, use the <code>&lt;del&gt;...&lt;/del&gt;</code> tag. <del>This section of text is meant to be treated as deleted text.</del></p>
<h3>Strikethrough Text</h3>
<p>For indicating blocks of text that are no longer correct, accurate or relevant, use the <code>&lt;s&gt;...&lt;/s&gt;</code> tag. <s>This section of text is meant to be treated as no longer correct, accurate or relevant.</s></p>
<h3>Inserted Text</h3>
<p>For indicating additions to the document, use the <code>&lt;ins&gt;...&lt;/ins&gt;</code> tag. <ins>This section of text is meant to be treated as an addition to the document.</ins></p>
<h3>Underlined Text</h3>
<p>To underline text, use the <code>&lt;u&gt;...&lt;/u&gt;</code> tag.</p>
<p><strong>Note:</strong> Try to avoid using underlined text on the web as users will most likely think that the underlined text is hyperlink, click on it and be disappointed ( or even irritated ). <u>How do we know?</u>  Common sense my friend, common sense.</p>
<h3>Emphasis Text</h3>
<p><code>&lt;em&gt;...&lt;/em&gt;</code></p>
<h3>Italic Text</h3>
<p><code>&lt;i&gt;...&lt;/i&gt;</code></p>
<h3>Strong Text</h3>
<p><code>&lt;strong&gt;...&lt;/strong&gt;</code></p>
<h3>Bold Text</h3>
<p>To make text bold, you may use the <code>&lt;b&gt;...&lt;/b&gt;</code> tag. <b>Just like this!</b></p> 
<h3>Small Text</h3>
<p><code>&lt;small&gt;...&lt;/small&gt;</code></p>
<h3>Abbreviations</h3>
<p>Use of the <code>&lt;abbr&gt;...&lt;/abbr&gt;</code> or <code>&lt;abbr title=""&gt;...&lt;/abbr&gt;</code> tag creates an element that identifies abbreviations and acronyms and optionally provides a full description for them. If present, the title attribute must contain this full description and nothing else. For example, <abbr title="abbreviation">abbr</abbr> is an abbreviation of abbreviation and <abbr>FTP</abbr> (no title attribute added) is an abbreviation of File Transfer Protocol.</p>
<p>Add <code>.initialism</code> to an abbreviation for a slightly smaller font-size. For example, <abbr title="Hypertext Markup Language" class="initialism">HTML</abbr> is an abbreviation of Hypertext Markup Language</p>
<p><small>Note: The <code>&lt;acronym&gt;...&lt;/acronym&gt;</code> tag is deprecated, yet it would be prudent to include styles for this element for backwards compatibility. For example <acronym title="Three Letter Acronym">TLA</acronym> and <acronym title="Object-oriented Programming">OOP</acronym>.</small></p>
<h3>Code Elements</h3>
<p>Inline code may be marked up using the <code>&lt;code&gt;...&lt;/code&gt;</code> tags.</p>
<h3>Inline Heading Styles</h3>
<p>You may use <code>class="h1"</code>, <code>class="h2"</code> etc. ..... For example <span class="h2">this section of text is styled h2</span> and <span class="h3">this section is styled as h3</span>.</p>
<p><small>Note: This section is incomplete, a better explanation is required.</small></p>


<h2>Preformatted text</h2>

<p><code>&lt;pre&gt;...&lt;/pre&gt;</code> Here is some preformatted text. Monospace is the norm.</p>
<pre>ooooooooooooooooo
_  _ ___ ___ ___ 
|__|  |   |  |__]
|  |  |   |  |   

kkkkkkkkkkkkkkkkk</pre>

<p>Code is often displayed within a preformatted text block, you will see this in use later on this page. <code>&lt;pre&gt;&lt;code&gt;...&lt;/code&gt;&lt;/pre&gt;</code></p>
<pre><code>$name = 'Graham';
if ( 'Graham' == $name )
    echo 'My name is Graham';</code></pre>
<h2>Blockquote</h2>
<blockquote>This is a block of quoted text</blockquote>
	
<h2>Keyboard</h2>
<p>Use <code>&lt;kbd&gt;...&lt;/kbd&gt;</code> to represent keyboard input keys. Additional styling is available by adding <code>class="highlight"</code>. For example <code>&lt;kbd class="highlight"&gt;Delete&lt;/kbd&gt;</code></p>
<p><kbd>Q</kbd><kbd><a href="#">W</a></kbd><kbd>E</kbd><kbd>R</kbd><kbd>T</kbd><kbd>Y</kbd><kbd>&larr;</kbd><kbd>&rarr;</kbd><br /><kbd>Ctrl</kbd><kbd>Alt</kbd><kbd class="highlight">Delete</kbd><kbd>NumLock</kbd></p>

<h2>Subscript</h2>
<p><code>&lt;sub&gt;...&lt;/sub&gt;</code> defines subscripted text. For example: C<sub>6</sub>H<sub>12</sub>O<sub>6</sub></p>

<h2>Superscript</h2>
<p><code>&lt;sup&gt;...&lt;/sup&gt;</code> defines superscripted text. For example:<br />7<sup>2</sup> + 4<sup>3</sup> = ??<br />On the 1<sup>st</sup>, 2<sup>nd</sup>, 3<sup>rd</sup> and 4<sup>th</sup> of each month</p>


<h2>Tables</h2>
<p>Ever wondered why the table caption is placed above the table? Read this <a href="http://tex.stackexchange.com/questions/3243/why-should-a-table-caption-be-placed-above-the-table" target="_blank" rel="nofollow">question and answer</a> on Tex Stack Exchange</p>



<h3>Default Table</h3>
<p><code>&lt;table&gt;...&lt;/table&gt;</code></p>
<div class="table-responsive">
<table>
  <caption>The caption for this table</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
  </tbody>
</table>

</div>


<h3>Striped Table Rows</h3>

<p><code>&lt;table class="table-striped"&gt;...&lt;/table&gt;</code> to add zebra-striping to any table row within the <code>&lt;tbody&gt;</code></p>
<div class="table-responsive">
<table class="table-striped">
  <caption>The caption for this table</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
  </tbody>
</table>
</div>



<h3>Bordered Table</h3>
<p><code>&lt;table class="table-bordered"&gt;...&lt;/table&gt;</code></p>
<div class="table-responsive">
<table class="table-bordered">
  <caption>The caption for this table</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
  </tbody>
</table>
</div>



<h3>Hover Rows</h3>
<p><code>&lt;table class="table-hover"&gt;...&lt;/table&gt;</code> to enable a hover state on table rows within a <code>&lt;tbody&gt;</code></p>

<div class="table-responsive">
<table class="table-hover">
  <caption>The caption for this table</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
  </tbody>
</table>
</div>






<h3>Condensed Table</h3>
<p><code>&lt;table class="table-condensed"&gt;...&lt;/table&gt;</code> Make tables more compact by cutting cell padding and possibly reducing font-size.</p>

<div class="table-responsive">
<table class="table-condensed">
  <caption>The caption for this table</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
  </tbody>
</table>
</div>


<h3>Combined Table Styles</h3>
<p>e.g. <code>&lt;table class="table-bordered table-condensed"&gt;...&lt;/table&gt;</code></p>

<div class="table-responsive">
<table class="table-bordered table-condensed">
  <caption>The caption for this table</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
      <th>Heading</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
      <td>Data</td>
    </tr>
  </tbody>
</table>
</div>


<h3>Responsive Tables</h3>
<p>Create responsive tables by wrapping any <code>&lt;table&gt;</code> in <code>.table-responsive</code> to make them scroll horizontally on small devices. When viewing on  larger devices, you will not see any difference. <code>&lt;div class="table-responsive"&gt;&lt;table&gt;...&lt;/table&gt;&lt;/div&gt;</code></p>

<div class="table-responsive">
  <table class="table-bordered table-condensed table-striped table-hover">
    <caption>The caption for this table</caption>
    <thead>
      <tr>
        <th>#</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
        <th>Heading</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
      </tr>
      <tr>
        <th scope="row">2</th>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
      </tr>
      <tr>
        <th scope="row">3</th>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
        <td>Data</td>
      </tr>
    </tbody>
  </table>
</div>

<h2>Definition Lists</h2>
<code></code>

<h2>Buttons</h2>
<div class="table-responsive">
<table class="table-bordered">
<thead>
<tr>
<th>Button</th>
<th>Role</th>
<th>Class</th>
</tr>
</thead>
<tbody>
<tr>
<td><button>Default</button></td>
<td>Default</td>
<td><code>&lt;button&gt;...&lt;/button&gt;</code></td>
</tr>
<tr>
<td><a class="button">A link</a></td>
<td>Default</td>
<td><code>&lt;a class="button"&gt;...&lt;/a&gt;</code> A link styled to look like a button</td>
</tr>
<tr>
<td><a class="button button-primary">Primary</a></td>
<td>Primary / Call to action</td>
<td><code>&lt;a class="button button-primary"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-secondary">Secondary</a></td>
<td>Secondary</td>
<td><code>&lt;a class="button-secondary"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button disabled">Disabled</a></td>
<td>Disabled</td>
<td><code>&lt;a class="button disabled"&gt;...&lt;/a&gt;</code> <code>&lt;button disabled="disabled"&gt;...&lt;/button&gt;</code></td>
</tr>
<tr>
<td><a class="button button-success">Success</a></td>
<td>Success</td>
<td><code>&lt;a class="button button-success"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-info">Info</a></td>
<td>Info</td>
<td><code>&lt;a class="button button-info"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-warning">Warning</a></td>
<td>Warning</td>
<td><code>&lt;a class="button button-warning"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-danger">Danger</a></td>
<td>Danger</td>
<td><code>&lt;a class="button button-danger"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-xs">Extra small</a></td>
<td>Extra small</td>
<td><code>&lt;a class="button button-xs"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-sm">Small</a></td>
<td>Small</td>
<td><code>&lt;a class="button button-sm"&gt;...&lt;/a&gt;</code></td>
</tr>
<tr>
<td><a class="button button-lg">Large</a></td>
<td>Large</td>
<td><code>&lt;a class="button button-lg"&gt;...&lt;/a&gt;</code></td>
</tr>

</tbody>
</table>
</div>


<h2>Alerts</h2>

<p>Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.</p>

<h3>No default class</h3>

<p>Alerts don't have default classes, only base and modifier classes. A default gray alert doesn't make too much sense, so you're required to specify a type via contextual class. Choose from success, info, warning, or danger.</p>

<div class="alert alert-success" role="alert">
  <strong>Well done!</strong> You successfully read this important alert message. <a href="#">This is a link</a>
</div>

<div class="alert alert-info" role="alert">
  <strong>Heads up!</strong> This alert needs your attention, but it's not super important. <a href="#">This is a link</a>
</div>

<div class="alert alert-warning" role="alert">
  <strong>Warning!</strong> Better check yourself, you're not looking too good. <a href="#">This is a link</a>
</div>

<div class="alert alert-danger" role="alert">
  <strong>Oh snap!</strong> Change a few things up and try submitting again. <a href="#">This is a link</a>
</div>
  
<pre><code>&lt;div class="alert alert-success" role="alert"&gt;...&lt;/div&gt;
&lt;div class="alert alert-info" role="alert"&gt;...&lt;/div&gt;
&lt;div class="alert alert-warning" role="alert"&gt;...&lt;/div&gt;
&lt;div class="alert alert-danger" role="alert"&gt;...&lt;/div&gt;
</code></pre>

<h2>Forms</h2>

<form>
<fieldset>
<legend>Legend</legend>
<p>
  <label for="exampleInputText">Text</label>
  <input type="text" class="form-control" id="exampleInputText" placeholder="Text">
</p>
<p>
    <label for="exampleInputEmail">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email">
</p>
<p>
  <label for="exampleInputPassword">Password</label>
  <input type="password" class="form-control" id="exampleInputPassword" placeholder="Password">
</p>
<p>
  <label for="exampleInputColor">Colour</label>
  <input type="color" class="form-control" id="exampleInputColor">
</p>
<p>
<label for="select-example">Select one</label>
<select id="select-example">
  <option>1</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
</select>
</p>
<p>
<label for="select-multiple">Select multiple</label>
<select multiple size="5" id="select-multiple">
  <option>Ford</option>
  <option>Renault</option>
  <option>Toyota</option>
  <option>Triumph</option>
  <option>Volvo</option>
</select>
</p>
<p>
<label for="textarea">Text Area</label>
<textarea placeholder="What's happening?"></textarea>
</p>
<p>
	<label for="exampleInputFile">File input</label>
	<input type="file" id="exampleInputFile">
	<p class="help-block">Example block-level help text here.</p>
</p>
</fieldset>
<div class="checkbox">
	<label>
		<input type="checkbox"> Check me out
	</label>
</div>
<button type="submit">Submit</button>
</form>
<?php

$html = ob_get_clean();
// trim compacted html and return
return trim( preg_replace('#>\s+<#s', '><', $html) ); 
}