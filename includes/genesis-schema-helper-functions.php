<?php

if ( !defined( 'ABSPATH' ) ) exit;

// See https://yoast.com/schema-org-genesis-2-0/
// http://www.101cookingfortwo.com/removing-genesis-schema/
// http://www.rfmeier.net/using-genesis_markup-with-html5-in-genesis-2-0/ - it's all about the context


// Remove the schema markup from an element
function phut_core_schema_empty( $attr ) {
	$attr['itemtype'] = '';
	$attr['itemprop'] = '';
	$attr['itemscope'] = '';
	return $attr;
}
// Change the schema type of an element to Product
function phut_core_schema_product( $attr ) {
	$attr['itemtype'] = 'http://schema.org/Product';
	$attr['itemprop'] = '';
	$attr['itemscope'] = 'itemscope';
	return $attr;
}
// Change the schema type of an element to Event
function phut_core_schema_event( $attr ) {
	$attr['itemtype'] = 'http://schema.org/Event';
	$attr['itemprop'] = '';
	$attr['itemscope'] = 'itemscope';
	return $attr;
}
// Change the schema type of an element to Place
function phut_core_schema_place( $attr ) {
	$attr['itemtype'] = 'http://schema.org/Place';
	$attr['itemprop'] = '';
	$attr['itemscope'] = 'itemscope';
	return $attr;
}

// Change the schema type of an element to Job Posting
function phut_core_schema_job( $attr ) {
	$attr['itemtype'] = 'http://schema.org/JobPosting';
	$attr['itemprop'] = '';
	$attr['itemscope'] = 'itemscope';
	return $attr;
}

// Change the schema type of an element to recipe
function phut_core_schema_recipe( $attr ) {
	$attr['itemtype'] = 'http://schema.org/Recipe';
	$attr['itemprop'] = '';
	$attr['itemscope'] = 'itemscope';
	return $attr;
}

// Change the schema type of an element to Review
// Make sure the itemprop is set to review so this can be used in conjunction with Event or Product
function phut_core_schema_review( $attr ) {
	$attr['itemtype'] = 'http://schema.org/Review';
	$attr['itemprop'] = 'review';
	$attr['itemscope'] = 'itemscope';
	return $attr;
}
// Set the itemprop of an element to description
function phut_core_itemprop_description( $attr ) {
	$attr['itemprop'] = 'description';
	return $attr;
}
// Set the itemprop of an element to name
function phut_core_itemprop_name( $attr ) {
	$attr['itemprop'] = 'name';
	return $attr;
}
// Set the itemprop of an element to title
function phut_core_itemprop_title( $attr ) {
	$attr['itemprop'] = 'title';
	return $attr;
}
// Remove the rel="author" and change it to itemprop="author" as the Structured Data Testing Tool doesn't understand 
// rel="author" in relation to Schema, even though it should according to the spec.
function phut_core_author_schema( $output ) {
	return str_replace( 'rel="author"', 'itemprop="author"', $output );
}
// Add the url itemprop to the URL of the entry
function phut_core_title_link_schema( $output ) {
	return str_replace( 'rel="bookmark"', 'rel="bookmark" itemprop="url"', $output );
}