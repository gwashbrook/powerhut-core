<?php
/*
Plugin Name: Powerhut Core
Plugin URI: https://bitbucket.org/gwashbrook/powerhut-core
Description:  
Author: Graham Washbrook
Author URI: https://powerhut.net/
Version: 1.0.26
*/

// WooCommerce snippets https://docs.woocommerce.com/documentation/plugins/woocommerce/woocommerce-codex/snippets/

if ( !defined( 'ABSPATH' ) ) exit;

if( !defined( 'PHUT_CORE_VER' ) ) define( 'PHUT_CORE_VER', '1.0.26' );

// Require Genesis schema helper functions
require_once( dirname( __FILE__ ) . '/includes/genesis-schema-helper-functions.php');

// Require [phut_css_check] shortcode
require_once( dirname( __FILE__ ) . '/includes/powerhut-css-check.php');

// Require widgets
// require_once( dirname( __FILE__ ) . '/widgets/widgets.php');

// Selectively enqueue admin scripts
add_action( 'admin_enqueue_scripts', 'phut_core_admin_scripts_styles' );
function phut_core_admin_scripts_styles( $hook ) {

	if ( 'toplevel_page_phut-core' != $hook ) return;

	wp_enqueue_script( 'phut_core_admin_script', plugin_dir_url( __FILE__ ) . 'admin/js/phut-core-admin.js', array('jquery-ui-tabs'), PHUT_CORE_VER );

	wp_register_style( 'phut_core_admin_style', plugin_dir_url( __FILE__ ) . 'admin/css/phut-core-admin.css', false, PHUT_CORE_VER );
	wp_enqueue_style( 'phut_core_admin_style' );	

}


add_action( 'genesis_theme_settings_metaboxes', 'phut_core_genesis_metaboxes' );
function phut_core_genesis_metaboxes( $_genesis_theme_settings_page_hook ) {

	$phut_core = get_option('phut_core');

	// Custom Feeds
	if( isset( $phut_core['remove_custom_feeds_mb'] ) )
		remove_meta_box( 'genesis-theme-settings-feeds', $_genesis_theme_settings_page_hook, 'main' );

	// Blog Page Template
	if( isset( $phut_core['remove_blog_page_template_mb'] ) )
		remove_meta_box( 'genesis-theme-settings-blogpage', $_genesis_theme_settings_page_hook, 'main' );

	// Navigation
	if( isset( $phut_core['remove_navigation_mb'] ) )
		remove_meta_box( 'genesis-theme-settings-nav', $_genesis_theme_settings_page_hook, 'main' );

	// Color Style Selector
	if( isset( $phut_core['remove_color_style_mb'] ) )
		remove_meta_box( 'genesis-theme-settings-style-selector', $_genesis_theme_settings_page_hook, 'main' );

}


add_action( 'widgets_init','phut_core_widgets_init', 11 );
function phut_core_widgets_init() {

	$phut_core = get_option('phut_core');

	if( isset( $phut_core['remove_genesis_user_profile_widget'] ))
		unregister_widget( 'Genesis_User_Profile_Widget' );

	if( isset( $phut_core['remove_genesis_featured_page_widget'] ))
		unregister_widget( 'Genesis_Featured_Page' );

	if( isset( $phut_core['remove_genesis_featured_post_widget'] ))
		unregister_widget( 'Genesis_Featured_Post' );

	if( isset( $phut_core['remove_wp_widget_calendar'] ))
		unregister_widget( 'WP_Widget_Calendar' );

	if( isset( $phut_core['remove_wp_widget_meta'] ))
		unregister_widget( 'WP_Widget_Meta' );

	if( isset( $phut_core['remove_wp_widget_rss'] ))
		unregister_widget( 'WP_Widget_RSS' );

	if( isset( $phut_core['remove_wp_widget_pages'] ))
		unregister_widget( 'WP_Widget_Pages' );

	if( isset( $phut_core['remove_wp_widget_recent_comments'] ))
		unregister_widget( 'WP_Widget_Recent_Comments' );

	if( isset( $phut_core['remove_wp_widget_image'] ))
		unregister_widget( 'WP_Widget_Media_Image' );	

	if( isset( $phut_core['remove_wp_widget_audio'] ))
		unregister_widget( 'WP_Widget_Media_Audio' );

	if( isset( $phut_core['remove_wp_widget_video'] ))
		unregister_widget( 'WP_Widget_Media_Video' );

	if( isset( $phut_core['remove_wp_widget_tagcloud'] ))
		unregister_widget( 'WP_Widget_Tag_Cloud' );

}


//* Init
add_action('init','phut_core_init');
function phut_core_init() {

	global $wp_embed;

	$phut_core = get_option('phut_core');

	/* Powerhut Core
	----------------------------------------------------------- */
	if( remove_all_filters( 'admin_footer_text' ) )
		add_filter('admin_footer_text', 'phut_core_admin_footer_text');

	/* WordPress
	----------------------------------------------------------- */

	if( isset( $phut_core['remove_rsd_link'] ) )
		remove_action('wp_head', 'rsd_link');	// Remove Really Simple Discovery link

	if( isset( $phut_core['remove_login_shake'] ) )
		add_action( 'login_head','phut_core_remove_login_shake' );

	if( isset( $phut_core['remove_wlwmanifest_link'] ) )
		remove_action( 'wp_head', 'wlwmanifest_link' ); // Remove rarely used Windows Live Writer link

	if( isset( $phut_core['remove_wp_ver'] ) ) {
		remove_action( 'wp_head', 'wp_generator' ); // Remove WP version from header source code
		add_filter( 'style_loader_src', 'phut_core_remove_wpver', 99999 );
		add_filter( 'script_loader_src', 'phut_core_remove_wpver', 99999 );
	}

	if( isset( $phut_core['remove_welcome_panel'] ) )
		add_action( 'wp_dashboard_setup', 'phut_core_remove_wp_welcome_panel' );

	if( isset( $phut_core['remove_wp_news'] ) )
		add_action( 'admin_init', 'phut_core_remove_wp_news' );		

	if( isset( $phut_core['remove_wp_plugins'] ) )
		add_action( 'admin_init', 'phut_core_remove_wp_plugin_notifications' );		

	if( isset( $phut_core['no_self_ping'] ) )
		add_action( 'pre_ping', 'phut_core_no_self_ping' );

	if( isset( $phut_core['remove_wp_logo'] ) )
		add_action( 'admin_bar_menu', 'phut_core_remove_wp_logo', 999 );

	if( isset( $phut_core['disable_emojis'] ) )
		add_action( 'init', 'phut_core_disable_emojis', 999 );

	if( isset( $phut_core['remove_adminbar_search'] ) )
		add_action( 'admin_bar_menu','phut_core_remove_adminbar_search', 999 );								

	if( isset( $phut_core['disable_xmlrpc'] ) )
		add_filter( 'xmlrpc_enabled', '__return_false' ); // http://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/

	if( isset( $phut_core['enable_widget_text_shortcodes'] ) )			
		add_filter( 'widget_text', 'do_shortcode' );

	if( isset( $phut_core['enable_link_manager'] ) )
		add_filter( 'pre_option_link_manager_enabled', '__return_true' );	

	if( isset( $phut_core['remove_edit_post_link'] ) )
		add_filter( 'edit_post_link', '__return_false' );

	if( isset( $phut_core['ajaxurl_frontend'] ) )
		add_action( 'wp_head', 'phut_core_ajaxurl_frontend' );


	/* Genesis
		----------------------------------------------------------- */

	if( isset( $phut_core['remove_genesis_page_templates'] ) )
		add_filter( 'theme_page_templates', 'phut_core_remove_genesis_page_templates' );

	if( isset( $phut_core['enable_archive_intro_shortcodes'] ) ) {
		add_filter( 'genesis_term_intro_text_output', 'do_shortcode' );
		add_filter( 'genesis_cpt_archive_intro_text_output', 'do_shortcode' );
		add_filter( 'genesis_author_intro_text_output', 'do_shortcode' );
	}

	if( isset( $phut_core['enable_archive_intro_embeds'] )) {
		add_filter( 'genesis_term_intro_text_output', array( $wp_embed, 'autoembed' ) );
		add_filter( 'genesis_cpt_archive_intro_text_output', array( $wp_embed, 'autoembed') );
		add_filter( 'genesis_author_intro_text_output', array( $wp_embed, 'autoembed') );
	}


	/* WooCommerce
	----------------------------------------------------------- */
	if( isset( $phut_core['wc_remove_related']))
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 ); // Remove related products display

	if( isset( $phut_core['wc_remove_upsells']))
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 ); // Remove upsell products display	

	if( isset( $phut_core['wc_disable_css']))
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		
	add_filter( 'woocommerce_product_tabs','phut_core_wc_product_tabs', 98 );
	function phut_core_wc_product_tabs( $tabs ){
		
		$phut_core = get_option('phut_core');
		
		if( isset( $phut_core['wc_remove_desc_tab']))
			unset( $tabs['description'] );

		if( isset( $phut_core['wc_remove_reviews_tab']))
			unset( $tabs['reviews'] );

		if( isset( $phut_core['wc_remove_info_tab']))
			unset( $tabs['additional_information'] );
	
		return $tabs;
	}

	if( isset( $phut_core['wc_remove_subcat_count']))
		add_filter( 'woocommerce_subcategory_count_html', '__return_empty_string' );
	
	if( isset( $phut_core['wc_remove_page_title']))
		add_filter( 'woocommerce_show_page_title', '__return_false' );

	/* Plugins
	----------------------------------------------------------- */

	// Contact form 7
	if( isset( $phut_core['disable_cf7_css'] ) )
		add_filter( 'wpcf7_load_css', '__return_false' );

	if( isset( $phut_core['disable_cf7_js'] ) )
		add_filter( 'wpcf7_load_js', '__return_false' );	

	// TablePress
	if( isset( $phut_core['disable_tablepress_css'] ) )
		add_filter( 'tablepress_use_default_css', '__return_false' );

	if( isset( $phut_core['remove_tablepress_edit_link'] ) )
		add_filter( 'tablepress_edit_link_below_table', '__return_false' );

	// Gravity Forms
	if( isset( $phut_core['enable_gf_label_visibility'] ) )
		add_filter( 'gform_enable_field_label_visibility_settings','__return_true' );

	// LayerSlider
	if( isset( $phut_core['remove_ls_generator'] ) )
		add_filter( 'ls_meta_generator', '__return_empty_string' );

	// Advanced Custom Fields
	if( isset( $phut_core['hide_acf_admin'] ) )
		add_filter('acf/settings/show_admin', '__return_false');
}


/**
 * Adds ajaxurl to front end
 */
function phut_core_ajaxurl_frontend(){ ?>
<script type="text/javascript">var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';</script>
<?php }


/**
 * Removes Genesis page templates
 */
function phut_core_remove_genesis_page_templates( $page_templates ) {
	unset( $page_templates['page_archive.php'] );
	unset( $page_templates['page_blog.php'] );
	return $page_templates;
}


/**
 * Removes search from admin bar
 *
 */
function phut_core_remove_adminbar_search( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'search' );
}

/**
 * Disables emojii
 * See http://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2
 */
function phut_core_disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'phut_core_disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'phut_core_disable_emojis_remove_dns_prefetch', 10, 2 );
}

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array  Difference betwen the two arrays
 */
function phut_core_disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}


/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param  array  $urls          URLs to print for resource hints.
 * @param  string $relation_type The relation type the URLs are printed for.
 * @return array                 Difference betwen the two arrays.
 */
function phut_core_disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}
	return $urls;
}


//* Adds admin footer links
function phut_core_admin_footer_text (){
	echo '<span id="phut-footer-thankyou">PhutCore v' . PHUT_CORE_VER . '&nbsp;<a href="https://powerhut.net/" target="_blank" title="Wonderful WordPress Websites">Powerhut</a>&nbsp;<span class="dashicons dashicons-heart"></span>&nbsp;<a href="https://wordpress.org/" target="_blank">WordPress</a>&nbsp;</span>';
}

//* Removes login shake
function phut_core_remove_login_shake() {
	remove_action('login_head', 'wp_shake_js', 12);
} 

//* Removes WP version params from any enqueued scripts/styles
function phut_core_remove_wpver( $src ) {
	if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}

//* Removes WordPress welcome panel
function phut_core_remove_wp_welcome_panel() {
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
}

//* Removes WordPress news metabox
function phut_core_remove_wp_news(){
	remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
}

//* Removes wordpress.org plugin notifications
function phut_core_remove_wp_plugin_notifications(){
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );
}

//* Stops self pings
function phut_core_no_self_ping( &$links ) {
	$home = get_option( 'home' );
	foreach ( $links as $l => $link )
		if ( 0 === strpos( $link, $home ) )
			unset( $links[$l] );
}

//* Removes WordPress logo from admin bar
function phut_core_remove_wp_logo( $wp_admin_bar ){
	$wp_admin_bar->remove_node( 'wp-logo' );
}


add_action('admin_init', 'phut_core_options_init' );
function phut_core_options_init(){
	register_setting( 'phut_core_options', 'phut_core' );
}

add_action('admin_menu', 'phut_core_add_pages');

// Add menu pages
function phut_core_add_pages() {
	add_menu_page( 'Powerhut Core', 'Powerhut', 'manage_options', 'phut-core', 'phut_core_do_core_page_cb', 'dashicons-forms' );
	add_submenu_page( 'phut-core', 'Powerhut Core', 'Core Options', 'manage_options', 'phut-core');
}

// Draw the menu page itself
function phut_core_do_core_page_cb() { ?>

<div class="wrap">

<h2>Powerhut Core Options</h2>

<div id="phut-core-tabs">

<ul class="nav-tab-wrapper wp-clearfix">
	<li><a class="nav-tab" href="#tab-wordpress">WordPress</a></li>
	<li><a class="nav-tab" href="#tab-genesis">Genesis</a></li>
	<li><a class="nav-tab" href="#tab-woocommerce">WooCommerce</a></li>
	<li><a class="nav-tab" href="#tab-plugins">Plugins</a></li>
</ul>

<form method="post" action="options.php">

<?php
settings_fields('phut_core_options');
$phut_core = get_option('phut_core');
if( defined ('WP_DEBUG' ) && WP_DEBUG  ) {
	echo '<pre class="phut-debug">';
	print_r( $phut_core );
	echo '</pre>';
}
?>

<div id="tab-wordpress">
<table class="form-table">
              
<tr valign="top">
<th scope="row">Remove RSD links</th>
<td><input name="phut_core[remove_rsd_link]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_rsd_link'] ) ); ?> /></td>
<td>Remove Really Simple Discovery link</td>
</tr>
            
<tr valign="top">
<th scope="row">Remove login shake</th>
<td><input name="phut_core[remove_login_shake]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_login_shake'] ) ); ?> /></td>
<td>Stops the WordPress login box shaking when a login or registration error is noted</td>
</tr>

<tr valign="top">
<th scope="row">Remove wlwmanifest link</th>
<td><input name="phut_core[remove_wlwmanifest_link]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wlwmanifest_link'] ) ); ?> /></td>
<td>If you're not using <a href="https://en.wikipedia.org/wiki/Windows_Live_Writer" target="_blank">Windows Live Writer</a>, then you don't need this link</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP version</th>
<td><input name="phut_core[remove_wp_ver]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_ver'] ) ); ?> /></td>
<td>Helps to hide that you are using WordPress <strong>Version <?php echo get_bloginfo( 'version' ) ?></strong></td>
</tr>

<tr valign="top">
<th scope="row">Remove WP welcome</th>
<td><input name="phut_core[remove_welcome_panel]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_welcome_panel'] ) ); ?> /></td>
<td>Declutter: Removes the <strong>Welcome to WordPress!</strong> panel from the dashboard</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP news</th>
<td><input name="phut_core[remove_wp_news]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_news'] ) ); ?> /></td>
<td>Declutter: Removes the <strong>WordPress news</strong> panel from the dashboard</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP plugin news</th>
<td><input name="phut_core[remove_wp_plugins]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_plugins'] ) ); ?> /></td>
<td>Declutter: Removes the <strong>WordPress plugin news</strong> panel from the dashboard</td>
</tr>

<tr valign="top">
<th scope="row">no_self_ping</th>
<td><input name="phut_core[no_self_ping]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['no_self_ping'] ) ); ?> /></td>
<td>An interesting one, so more on this later. Basically, check this box</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP logo</th>
<td><input name="phut_core[remove_wp_logo]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_logo'] ) ); ?> /></td>
<td>Declutter: Removes the WordPress logo from the admin bar</td>
</tr>

<tr valign="top">
<th scope="row">Disable emojis</th>
<td><input name="phut_core[disable_emojis]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['disable_emojis'] ) ); ?> /></td>
<td>Code bloat: Disables the emoji functionality added in WordPress 4.2</td>
</tr>

<tr valign="top">
<th scope="row">Remove adminbar search</th>
<td><input name="phut_core[remove_adminbar_search]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_adminbar_search'] ) ); ?> /></td>
<td>Declutter: Removes the search form from the admin bar</td>
</tr>

<tr valign="top">
<th scope="row">Disable XML-RPC</th>
<td><input name="phut_core[disable_xmlrpc]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['disable_xmlrpc'] ) ); ?> /></td>
<td>Disable XML-RPC. See <a href="http://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/" target="_blank">http://www.wpbeginner.com/plugins/how-to-disable-xml-rpc-in-wordpress/</a></td>
</tr>

<tr valign="top">
<th scope="row">Enable shortcodes in text widgets</th>
<td><input name="phut_core[enable_widget_text_shortcodes]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['enable_widget_text_shortcodes'] ) ); ?> /></td>
<td>Allows use of WordPress shortcodes in text widgets</td>
</tr>

<tr valign="top">
<th scope="row">Enable link manager</th>
<td><input name="phut_core[enable_link_manager]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['enable_link_manager'] ) ); ?> /></td>
<td>Enables the link manager removed from fresh installs v3.5</td>
</tr>

<tr valign="top">
<th scope="row">Remove edit post link</th>
<td><input name="phut_core[remove_edit_post_link]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_edit_post_link'] ) ); ?> /></td>
<td>Removes the "Edit" link shown in some templates on the front end</td>
</tr>
 
<tr valign="top">
<th scope="row">Add ajaxurl to frontend</th>
<td><input name="phut_core[ajaxurl_frontend]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['ajaxurl_frontend'] ) ); ?> /></td>
<td>Adds ajaxurl to frontend</td>
</tr> 

<tr valign="top">
<th scope="row">Remove WP Calendar widget</th>
<td><input name="phut_core[remove_wp_widget_calendar]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_calendar'] ) ); ?> /></td>
<td>Removes <strong>Calendar</strong> widget from available widgets</td>
</tr>
              
<tr valign="top">
<th scope="row">Remove WP Meta widget</th>
<td><input name="phut_core[remove_wp_widget_meta]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_meta'] ) ); ?> /></td>
<td>Removes <strong>Meta</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP RSS widget</th>
<td><input name="phut_core[remove_wp_widget_rss]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_rss'] ) ); ?> /></td>
<td>Removes <strong>RSS</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP Pages widget</th>
<td><input name="phut_core[remove_wp_widget_pages]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_pages'] ) ); ?> /></td>
<td>Removes <strong>Pages</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP Recent Comments widget</th>
<td><input name="phut_core[remove_wp_widget_recent_comments]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_recent_comments'] ) ); ?> /></td>
<td>Removes <strong>Recent Comments</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP Tag Cloud widget</th>
<td><input name="phut_core[remove_wp_widget_tagcloud]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_tagcloud'] ) ); ?> /></td>
<td>Removes <strong>Tag Cloud</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP Image widget</th>
<td><input name="phut_core[remove_wp_widget_image]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_image'] ) ); ?> /></td>
<td>Removes <strong>Image</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP Audio widget</th>
<td><input name="phut_core[remove_wp_widget_audio]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_audio'] ) ); ?> /></td>
<td>Removes <strong>Audio</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove WP Video widget</th>
<td><input name="phut_core[remove_wp_widget_video]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_wp_widget_video'] ) ); ?> /></td>
<td>Removes <strong>Video</strong> widget from available widgets</td>
</tr>

</table>
</div><!--//#tab-wordpress-->
		
<div id="tab-genesis">
<table class="form-table">

<tr valign="top">
<th scope="row">Remove Genesis templates</th>
<td><input name="phut_core[remove_genesis_page_templates]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_genesis_page_templates'] ) ); ?> /></td>
<td>See <a href="http://www.billerickson.net/remove-genesis-page-templates/" target="_blank">http://www.billerickson.net/remove-genesis-page-templates/</a></td>
</tr>

<tr valign="top">
<th scope="row">Enable shortcodes in Genesis Archive Intro</th>
<td><input name="phut_core[enable_archive_intro_shortcodes]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['enable_archive_intro_shortcodes'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Enable oEmbed in Genesis Archive Intro</th>
<td><input name="phut_core[enable_archive_intro_embeds]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['enable_archive_intro_embeds'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Remove Custom Feeds metabox from theme settings</th>
<td><input name="phut_core[remove_custom_feeds_mb]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_custom_feeds_mb'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Remove Blog Page Template metabox from theme settings</th>
<td><input name="phut_core[remove_blog_page_template_mb]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_blog_page_template_mb'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Remove Navigation metabox from theme settings</th>
<td><input name="phut_core[remove_navigation_mb]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_navigation_mb'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Remove Color Style metabox from theme settings</th>
<td><input name="phut_core[remove_color_style_mb]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_color_style_mb'] ) ); ?> /></td>
<td>Also found in Appearance > Customise</td>
</tr>

<tr valign="top">
<th scope="row">Remove Featured Posts widget</th>
<td><input name="phut_core[remove_genesis_featured_post_widget]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_genesis_featured_post_widget'] ) ); ?> /></td>
<td>Removes <strong>Genesis - Featured Posts</strong> widget from available widgets</td>
</tr> 

<tr valign="top">
<th scope="row">Remove Featured Page widget</th>
<td><input name="phut_core[remove_genesis_featured_page_widget]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_genesis_featured_page_widget'] ) ); ?> /></td>
<td>Removes <strong>Genesis - Featured Page</strong> widget from available widgets</td>
</tr>

<tr valign="top">
<th scope="row">Remove User Profile widget</th>
<td><input name="phut_core[remove_genesis_user_profile_widget]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_genesis_user_profile_widget'] ) ); ?> /></td>
<td>Removes <strong>Genesis - User Profile</strong> widget from available widgets</td>
</tr>

</table>
</div><!--//#tab-genesis-->

<div id="tab-woocommerce">

<h3>WooCommerce</h3>

<table class="form-table">

<tr valign="top">
<th scope="row">Remove upsell products</th>
<td><input name="phut_core[wc_remove_upsells]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_upsells'] ) ); ?> /></td>
<td>Removes upsell products output from single product view</td>
</tr>

<tr valign="top">
<th scope="row">Remove related products</th>
<td><input name="phut_core[wc_remove_related]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_related'] ) ); ?> /></td>
<td>Removes related products output from single product view</td>
</tr>

<tr valign="top">
<th scope="row">Remove description tab</th>
<td><input name="phut_core[wc_remove_desc_tab]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_desc_tab'] ) ); ?> /></td>
<td>Removes product description tab from single product view</td>
</tr>

<tr valign="top">
<th scope="row">Remove reviews tab</th>
<td><input name="phut_core[wc_remove_reviews_tab]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_reviews_tab'] ) ); ?> /></td>
<td>Removes product reviews tab from single product view</td>
</tr>

<tr valign="top">
<th scope="row">Remove additional info tab</th>
<td><input name="phut_core[wc_remove_info_tab]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_info_tab'] ) ); ?> /></td>
<td>Removes additional product information tab from single product view</td>
</tr>

<tr valign="top">
<th scope="row">Remove sub-category count</th>
<td><input name="phut_core[wc_remove_subcat_count]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_subcat_count'] ) ); ?> /></td>
<td>Removes sub-category product count from product archives</td>
</tr>

<tr valign="top">
<th scope="row">Remove page titles</th>
<td><input name="phut_core[wc_remove_page_title]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_remove_page_title'] ) ); ?> /></td>
<td>Removes page titles from archive views</td>
</tr>

<tr valign="top">
<th scope="row">Disable WooCommerce CSS</th>
<td><input name="phut_core[wc_disable_css]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['wc_disable_css'] ) ); ?> /></td>
<td>Disables all WooCommerce CSS</td>
</tr>

</table>
</div><!--//#tab-woocommerce-->

<div id="tab-plugins">

<h3>Contact Form 7</h3>

<table class="form-table">

<tr valign="top">
<th scope="row">Disable CSS</th>
<td><input name="phut_core[disable_cf7_css]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['disable_cf7_css'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Disable JS</th>
<td><input name="phut_core[disable_cf7_js]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['disable_cf7_js'] ) ); ?> /></td>
<td></td>
</tr>

</table>

<h3>Layer Slider</h3>
<table class="form-table">

<tr valign="top">
<th scope="row">Remove generator meta</th>
<td><input name="phut_core[remove_ls_generator]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_ls_generator'] ) ); ?> /></td>
<td></td>
</tr>
 
</table>

<h3>TablePress</h3>
<table class="form-table">

<tr valign="top">
<th scope="row">Disable CSS</th>
<td><input name="phut_core[disable_tablepress_css]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['disable_tablepress_css'] ) ); ?> /></td>
<td></td>
</tr>

<tr valign="top">
<th scope="row">Remove edit table link</th>
<td><input name="phut_core[remove_tablepress_edit_link]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['remove_tablepress_edit_link'] ) ); ?> /></td>
<td></td>
</tr>

</table>

<h3>Gravity Forms</h3>
<table class="form-table">

<tr valign="top">
<th scope="row">Enable label visibility settings</th>
<td><input name="phut_core[enable_gf_label_visibility]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['enable_gf_label_visibility'] ) ); ?> /></td>
<td></td>
</tr>

</table>

<h3>Advanced Custom Fields</h3>

<table class="form-table">

<tr valign="top">
<th scope="row">Hide ACF admin menu</th>
<td><input name="phut_core[hide_acf_admin]" type="checkbox" value="1" <?php checked( '1', isset( $phut_core['hide_acf_admin'] ) ); ?> /></td>
<td></td>
</tr>

</table>

</div><!--//#tab-plugins-->

<p class="submit">
	<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    <input type="reset" class="button-secondary" value="<?php _e('Reset') ?>" />
</p>

</form>

</div><!--//#phut-core-tabs-->
</div><!--//.wrap-->
	<?php	
}